import React from 'react';

// Renders the footer HTML
function Footer() {
    return (
        <footer>
            <h2>Contact Details</h2>
            <section>
                <h3>Researcher</h3>
                <p>Nathan Pope</p>
                <p>23700271@edgehill.ac.uk</p>
            </section>
            <section>
                <h3>Project Supervisor</h3>
                <p>Dr Hari Pandey</p>
                <p>Pandeyh@edgehill.ac.uk</p>
            </section>
            <p>Website designed and developed by Nathan Pope</p>
        </footer>
    );
}

export default Footer;