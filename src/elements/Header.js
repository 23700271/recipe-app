import React from 'react';
// Import React Router
import { NavLink } from 'react-router-dom';

// Import logo image
import logo from '../quacking-logo.svg';

// Renders the header with navbar NavLinks
function Header() {
    return (
        <header>
            {/* Navbar */}
            <nav>
                {/* Logo */}
                <NavLink to="/"><img src={logo} alt="logo" /></NavLink>
                <ul>
                    <li><NavLink exact to="/category">Recipes</NavLink></li>
                    <li><NavLink exact to="/favourites">Favourites</NavLink></li>
                    <li><NavLink exact to="/shopping-list">Shopping List</NavLink></li>
                </ul>
            </nav>
        </header>
    );
}

export default Header;