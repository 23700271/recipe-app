import React from 'react';

// Import react router
import { Link } from 'react-router-dom';

// Renders the list of recipes to display
class RecipesList extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        this.props.removeFavourite(e.target.name);
    }

    render() {
        let recipes = this.props.recipes;
        let images = this.props.images;
        let list = [];
    
        for (let i = 0; i < recipes.length; i++) {
            for (let j = 0; j < images.length; j++) {
                if (images[j].id === recipes[i].image_id) {
                    list.push(
                        <section className="favourite-recipe" key={recipes[i].id}>
                            <Link to={"/recipe/" + recipes[i].id}>
                                <img src={images[j].image_path} alt={images[j].description} />
                                <h3>{recipes[i].title}</h3>
                            </Link>
                            <button name={recipes[i].id} onClick={this.handleClick}>Remove</button>
                        </section>
                    );
                    break;
                }
            }
        }
    
        return list;
    }
}

class Favourites extends React.Component {
    constructor(props) {
        super(props);
        this.removeFavourite = this.removeFavourite.bind(this);

        // Set initial states
        this.state = {
            recipes: [],
            images: [],
            isEmpty: true
        }
    }

    async componentDidMount() {
        await this.fetchFavourites();
        if (Array.isArray(this.state.recipes) && this.state.recipes.length) {
            this.setState({ isEmpty: false });
        }
    }

    fetchFavourites() {
        const recipes = JSON.parse(localStorage.getItem('recipes'));
        const images = JSON.parse(localStorage.getItem('images'));
        if (Array.isArray(recipes) && recipes.length) {   
            this.setState({
                recipes: recipes,
                images: images
            });
        } else {
            this.setState({ isEmpty: true });
        }
    }

    removeFavourite(recId) {
        // let recId = props.id;
        // Get data from local storage
        let favList = JSON.parse(localStorage.getItem('favList'));
        let recipes = JSON.parse(localStorage.getItem('recipes'));
        let images = JSON.parse(localStorage.getItem('images'));
        let recipesIngredients = JSON.parse(localStorage.getItem('recipesIngredients'));
        let quantities = JSON.parse(localStorage.getItem('quantities'));
        let units = JSON.parse(localStorage.getItem('units'));
        let ingredients = JSON.parse(localStorage.getItem('ingredients'));
        let imgId;

        // Remove the recipe_ingredient for the recipe.
        // If an ingredient is not associated with any 
        // of the remaining recipe_ingredient tables then
        // remove it.
        
        // Remove from favList
        for (let i = 0; i < favList.length; i++) {
            if (favList[i] === recId) {
                favList.splice(i, 1);
                break;
            }
        }

        // Remove recipe
        for (let i = 0; i < recipes.length; i++) {
            if (recipes[i].id === recId) {
                // Get image id of recipe
                imgId = recipes[i].image_id;
                // Remove recipe from recipes list
                recipes.splice(i, 1);
                // End the loop
                break;
            }
        }

        // Remove image
        for (let i = 0; i < images.length; i++) {
            if (images[i].id === imgId) {
                images.splice(i, 1);
                break;
            }
        }

        // Remove associated recipe ingredients
        for (let i = recipesIngredients.length - 1; i >= 0; i--) {
            if (recipesIngredients[i].recipe_id === recId) {
                recipesIngredients.splice(i, 1);
            }
        }

        // Remove associated quantities (If not used by other favourite recipes)
        for (let i = quantities.length - 1; i >= 0; i--) {
            let isUsed = false;
            for (let j = 0; j < recipesIngredients.length; j++) {
                if (quantities[i].id === recipesIngredients[j].measurement_qty_id) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                quantities.splice(i, 1);
            }
        }

        // Remove associated units (If not used by other favourite recipes)
        for (let i = units.length - 1; i >= 0; i--) {
            let isUsed = false;
            for (let j = 0; j < recipesIngredients.length; j++) {
                if (units[i].id === recipesIngredients[j].measurement_unit_id) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                units.splice(i, 1);
            }
        }

        // Remove associated ingredients (If not used by other favourite recipes)
        for (let i = ingredients.length - 1; i >= 0; i--) {
            let isUsed = false;
            for (let j = 0; j < recipesIngredients.length; j++) {
                if (ingredients[i].id === recipesIngredients[j].ingredient_id) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                ingredients.splice(i, 1);
            }
        }
    
        // Stringify updated favourites list and set to local storage
        localStorage.setItem('favList', JSON.stringify(favList));
        // Recipe
        localStorage.setItem('recipes', JSON.stringify(recipes));
        // Image
        localStorage.setItem('images', JSON.stringify(images));
        // Recipe ingredients
        localStorage.setItem('recipesIngredients', JSON.stringify(recipesIngredients));
        // Quantities
        localStorage.setItem('quantities', JSON.stringify(quantities));
        // Units
        localStorage.setItem('units', JSON.stringify(units));
        // Ingredients
        localStorage.setItem('ingredients', JSON.stringify(ingredients));

        // Refresh state with updated favourites list data
        this.fetchFavourites();
    }

    // Renders the favourites page
    render() {
        // Inline logic checks if state isEmpty is true or false
        // If true show the empty list message
        // If false call the child component RecipesList. 
        // Pass props for recipes, images and the removeFavourite function (Allow parent function to be called by child component)
        return (
            <main className="pg-favourites">
                <h1>Favourites</h1>
                {this.state.isEmpty
                    ? <p>Your favourites list is empty.</p>
                    : <RecipesList 
                        recipes={this.state.recipes} 
                        images={this.state.images} 
                        removeFavourite={this.removeFavourite} />
                }
            </main>
        );
    }
}

export default Favourites;