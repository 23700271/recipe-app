// Import React framework
import React from 'react';

// Import React Router
import { Link, withRouter } from 'react-router-dom';

// Import custom hook for fetching JSON data
import useFetchJSON from '../../hooks/FetchJSON.js';

// Renders the categories recipes list
function RecipesList(props) {
    let recipes = props.recipes;
    let images = props.images;
    let recipe_list = [];
    
    // Loop through category recipes and images data
    // For each recipe loop through images to get the correct image data
    // Push HTML with recipe and image data to the recipe_list array
    for (let i = 0; i < recipes.length; i++) {
        for (let j = 0; j < images.length; j++) {
            if (recipes[i].image_id === images[j].id) {
                recipe_list.push(
                    <section key={recipes[i].id} >
                        <Link to={"/recipe/" + recipes[i].id}>
                            <img src={images[j].image_path} alt={images[j].description} />
                            <h3>{recipes[i].title}</h3>
                        </Link>
                    </section>
                );    
                break;            
            }
        }
    }
    // Return the rendered recipe_list sections
    return recipe_list;
}

// Renders the browse category page
class BrowseRecipeCategory extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            cat_name: "",
            cat_description: "",
            recipes: [],
            images: []
        }
    }

    async componentDidMount() {
        // Get category ID from URL ID parameter
        const catId = this.props.match.params.id;

        // Fetch category.json from online repository
        const categories = await useFetchJSON('category');

        // Get category data for selected category
        this.getCategory(categories, catId)

        // Fetch recipe_category.json from online repository
        const recipeCategory = await useFetchJSON('recipe_category');

        // Get recipe category data for selected category
        this.getCategoryRecipes(recipeCategory, catId);

        // Fetch recipe.json from online repository
        const recipe = await useFetchJSON('recipe');

        // Get recipes data for selected category
        this.getRecipes(recipe, recipeCategory);

        // Fetch image information from image.json
        const images = await useFetchJSON('image');

        // Get images for current recipes and update state
        this.getImages(images);
    }

    // Get the category data from category JSON using the cateogry id
    getCategory(categories, cat_id) {
        // Loop through categories JSON data
        for (let i = 0; i < categories.length; i++) {
            if (categories[i].id === cat_id) {
                // Update component state with category name and description
                this.setState({
                    cat_name: categories[i].name,
                    cat_description: categories[i].description
                });
            }
        }
    }

    // Gets the recide ids for the current category
    getCategoryRecipes(recipe_category, cat_id) {
        let categoryRecipes = [];
        // Loop through recipe_category linking table JSON data.
        // If recipe_category has the same category id as current category
        // then push the recipe_category data in the loop to the categoryRecipes array.
        for (let i = 0; i < recipe_category.length; i++) {
            if (recipe_category[i].category_id === cat_id) {
                categoryRecipes.push(recipe_category[i].recipe_id);
            }
        }
        // Update component state with category recipes data
        this.setState({recipe_category: categoryRecipes});
    }

    // Filter the recipes JSON data and get the category recipes
    getRecipes(allRecipes) {
        let filteredRecipes = [];
        let cat_rec = this.state.recipe_category;

        // Loops through recipes JSON data and checks if a recipe belongs to the category
        // Pushes recipe data that does belong to the category to the filtered recipes array
        for (let i = 0; i < allRecipes.length; i++) {
            for (let j = 0; j < cat_rec.length; j++) {
                if (allRecipes[i].id === cat_rec[j]) {
                    filteredRecipes.push(allRecipes[i]);
                }
            }
        }
        // Update state with the category recipes data
        this.setState({recipes: filteredRecipes});
    }

    // Filter the images JSON data to get the images for the category recipes
    getImages(images) {
        let recipes = this.state.recipes;
        let filteredImages = [];

        // Loops through state recipes and images JSON data.
        // If a state recipe image id is the same as the current image in the loop 
        // then push the image to the filtered images array.
        for (let i = 0; i < recipes.length; i++) {
            for (let j = 0; j < images.length; j++) {
                if (recipes[i].image_id === images[j].id) {
                    filteredImages.push(images[j]);
                    break;
                }
            }
        }
        // Update component state with filtered images array 
        this.setState({ images: filteredImages });
    }

    // Renders the browse category page
    // Calls the RecipesList child component with props for state recipes and images data
    render() {
        let name = this.state.cat_name;
        let description = this.state.cat_description;
        
        return (
            <main className="pg-browse-category">
                <h1>{name} Recipes</h1>
                <p>{description}</p>
                <RecipesList recipes={this.state.recipes} images={this.state.images} />
            </main>
        );
    }
}

// Export withRouter to allow access to URL parameters in BrowseRecipeCategory component
export default withRouter(BrowseRecipeCategory);