// Import React framework
import React from 'react';

// Import React Router
import { Link } from 'react-router-dom';

// Import custom hook for JSON data fetch
import useFetchJSON from '../../hooks/FetchJSON.js';

// Renders the categories list
function CategoriesList(props) {
    let categories = props.categories;
    let categories_list = [];

    // Loops through categories data and creates HTML section
    // Pushes the HTML section to the categories list array
    for (let i = 0; i < categories.length; i++) {
        categories_list.push(
            <section className="" key={categories[i].id}>
                <Link to={"/category/" + categories[i].id}>
                    <h2>{categories[i].name}</h2>
                    <p>{categories[i].description}</p>
                </Link>
            </section>
        );
    }
    // Returns the categories list of HTML sections to the parent component
    return categories_list;
}

// Renders the category overview page
class RecipeCategoryOverview extends React.Component {
    constructor() {
        super();
        this.state = {
            categories: []
        }
    }
    // When component has mounted fetch category JSON data and update component state
    async componentDidMount() {
        // Fetch category.json from online repository
        const categories = await useFetchJSON('category');
        // Set state of categories to fetched JSON category data
        this.setState({categories});
    }
    // Renders the component HTML. Calls the CategoriesList component with prop for categories state data
    render() {
        return (
            <main className="pg-category-overview">
                <h1>Recipe Categories</h1>
                <CategoriesList categories={this.state.categories} />
            </main>
        );
    }
}

export default RecipeCategoryOverview;
