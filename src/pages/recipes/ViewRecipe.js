// Import React framework
import React from 'react';

// Import React Router
import { withRouter } from 'react-router-dom';

// Import custom hook for JSON data fetch
import useFetchJSON from '../../hooks/FetchJSON.js';

// Renders recipe method list section
function Method(props) {
    let method = props.method;
    // Method string is split into an array where the \n new line is used
    let method_array = method.split("\n");
    let method_list = [];

    // Renders the list elements for the method
    for (let i = 0; i < method_array.length; i++) {
        method_list.push(
            <li key={i}>{method_array[i]}</li>
        );
    }
    // Returns rendered array as an ordered list
    return (
        <section>
            <h2>Method</h2>
            <ol className="ingr-method-lists">
                {method_list}
            </ol>
        </section>
    );
}

// Renders the 'Add To Shopping List' and 'Added' buttons. Handles logic for saving shopping list items to localStorage
class AddToShoppingList extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            ingredients: props.ingredients,
            hasAdded: false
        };
    }

    // Adds the recipes ingredients to the shopping list localStorage array
    updateShoppingList() {
        let ingredients = this.props.ingredients;
        let shopLsItems = JSON.parse(localStorage.getItem('shopLsItems'));

        if (!Array.isArray(shopLsItems)) {
            shopLsItems = [];
        }

        // Loop through recipe ingredients
        for (let i = 0; i < ingredients.length; i++) {
            let hasIngr = false;
            let qty = parseFloat(ingredients[i].qty.qty);
            if (ingredients[i].qty.qty === "") {
                qty = 1;
            }

            // If shopping list is not empty
            if (Array.isArray(shopLsItems) && shopLsItems.length) {
                // Loop through the shopping list items ingredients
                for (let j = 0; j < shopLsItems.length; j++) {
                    // If shopping list has recipe ingredient already then do not add it
                    if (shopLsItems[j].ingr.id === ingredients[i].ingr.id) {
                        hasIngr = true;
                        shopLsItems[j].qty += qty;
                        break;
                    }
                }
            }

            // If ingredient not in shopping list then create new item and push to shopping list
            if (!hasIngr) {
                let newItem = {
                    ingr: ingredients[i].ingr,
                    unit: ingredients[i].unit,
                    qty: qty
                }
                shopLsItems.push(newItem);
            }
        }
        
        // Save updated shopping list items to local storage
        localStorage.setItem('shopLsItems', JSON.stringify(shopLsItems));
        // Change state of hasAdded to true to swap 'Add To Shopping List' button to the disabled 'Added' button
        this.setState({ hasAdded: true });
    }

    // When 'Add To Shopping List' button clicked call the updateShoppingList function 
    // to add ingredients to shopping list.
    handleClick(event) {
        this.updateShoppingList();
        event.preventDefault();
    }

    // Renders the 'Add To Shopping List' button or the 'Added' disabled button if already clicked
    render() {
        return (
            <section className="btn-container">
                {this.state.hasAdded 
                    ? <button className="action-btn added-ingredients-btn" disabled>Added</button>
                    : <button className="action-btn add-ingredients-btn" onClick={this.handleClick}>Add To Shopping List</button>
                }
            </section>
        );
    }
}

// Component renders the ingredients list
function Ingredients(props) {
    // Get ingredients list from props
    const ingredients = props.ingredients;
    // Initialise empty list for rendered ingredient list items
    let ingredients_list = [];
    
    // Loop over the ingredients array and push HTML list elements to ingredients_list
    for (let i = 0; i < ingredients.length; i++) {
        let optTxt = "";
        // If the ingredient is optional then show string (optional)
        if (ingredients[i].optional) {
            optTxt = " (optional)"
        }
        // Push the new ingredient item to the ingredients_list
        ingredients_list.push(
            <li key={ingredients[i].ingr.id}>
                {ingredients[i].qty.qty + ingredients[i].unit.unit + " " + ingredients[i].ingr.name + optTxt}
            </li>
        );
    }

    // Return the HTML for ingredients list.
    // Call the AddToShoppingList component to render the 'Add To Shopping List' button child component
    return (
        <section>
            <h2>Ingredients</h2>
            <ul className="ingr-method-lists">
                {ingredients_list}
            </ul>
            <AddToShoppingList ingredients={props.ingredients} />
        </section>
    );
}

// Renders the top section of the recipes page
// Includes the image, prep and cook time, servings and description
function LeadingInfo(props) {
    const unit_mins = " mins";

    return (
        <section className="leading-info">
            <img src={props.image_path} alt={props.image_description} />
            <aside className="recipe-aside">
                <section>
                    <h3>Preparation Time</h3>
                    <p>{props.prep_time + unit_mins}</p>
                </section>
                <section>
                    <h3>Cooking Time</h3>
                    <p>{props.cook_time + unit_mins}</p>
                </section>
                <section>
                    <h3>Servings</h3>
                    <p>{props.servings}</p>
                </section>
            </aside>
            <p>{props.description}</p>
        </section>
    );
}

// Parent component for viewing a recipe which retrieves all data and distributes it to its children components as props
class ViewRecipe extends React.Component {
    constructor(props) {
        super(props);
        // Initial component state
        // rec_id is set using the URL parameter
        this.state = {
            rec_id: this.props.match.params.id,
            title: "",
            description: "",
            prep_time: 0,
            cook_time: 0,
            servings: 1,
            method: "",
            image_id: "",
            image_path: "",
            image_description: "",
            user_id: "",
            ingredients: [],
            isFav: false
        }

        // Binds the handleClickFavourite function to the component
        this.handleClickFavourite = this.handleClickFavourite.bind(this);
    }

    // Gets the necessary data to populate the page after component has mounted
    async componentDidMount() {
        const rec_id = this.state.rec_id;
        let favList = JSON.parse(localStorage.getItem('favList'));

        // Check if current recipe has been favourited and set state of isFav
        if (Array.isArray(favList) && favList.length) {
            for (let i = 0; i < favList.length; i++) {
                if (favList[i] === rec_id) {
                    this.setState({ isFav: true });
                    break;
                }
            }
        }

        if (!navigator.onLine) {
            for (let i = 0; i < favList.length; i++) {
                if (favList[i] === rec_id) {
                    let localRecipes = JSON.parse(localStorage.getItem('recipes'));
                    let localImages = JSON.parse(localStorage.getItem('images'));
                    let localRecipesIngredients = JSON.parse(localStorage.getItem('recipesIngredients'));
                    let localQuantities = JSON.parse(localStorage.getItem('quantities'));
                    let localUnits = JSON.parse(localStorage.getItem('units'));
                    let localIngredients = JSON.parse(localStorage.getItem('ingredients'));
                    
                    this.getRecipe(localRecipes, rec_id);
                    this.getImage(localImages);
                    this.getRecipeIngredients(rec_id, localRecipesIngredients, localQuantities, localUnits, localIngredients);
                    break;
                }
            }

        } else {
            // Fetch recipes from recipe.json
            const rec_json = await useFetchJSON('recipe');
            // Get current recipe and update state
            this.getRecipe(rec_json, rec_id);

            // Fetch image information from image.json
            const img_json = await useFetchJSON('image');
            // Get image for current recipe and update state for image_path and image_description
            this.getImage(img_json);

            // Fetch recipe ingredient information from recipe_ingredient.json
            const rec_ingr_json = await useFetchJSON('recipe_ingredient');
            // Fetch measurement quantity from measurement_quantity.json
            const qty_json = await useFetchJSON('measurement_quantity');
            // Fetch measurement unit from measurement_unit.json
            const unit_json = await useFetchJSON('measurement_unit');
            // Fetch ingredients from ingredient.json
            const ingr_json = await useFetchJSON('ingredient');
            // Get ingredients list for current recipe
            this.getRecipeIngredients(rec_id, rec_ingr_json, qty_json, unit_json, ingr_json); 
        }
    }

    // Finds the recipe data in the recipes JSON data and stores the required data in component state
    getRecipe(recipes, rec_id) {
        for (let i = 0; i < recipes.length; i++) {
            if (recipes[i].id === rec_id) {
                this.setState({
                    user_id: recipes[i].user_id,
                    title: recipes[i].title,
                    description: recipes[i].description,
                    prep_time: recipes[i].prep_time,
                    cook_time: recipes[i].cook_time,
                    servings: recipes[i].servings,
                    method: recipes[i].method,
                    image_id: recipes[i].image_id
                });
                break;
            }
        }
    }

    // Finds the image object for the recipe in the images JSON data and stores the required data in component state
    getImage(images) {
        const img_id = this.state.image_id;
        for (let i = 0; i < images.length; i++) {
            if (images[i].id === img_id) {
                this.setState({
                    image_path: images[i].image_path,
                    image_description: images[i].description
                });
                break;
            }
        }
    }

    // Constructs an ingredients list from JSON data and stores it in the component ingredients state
    getRecipeIngredients(rec_id, rec_ingr_list, qty_list, unit_list, ingr_list) {
        let ingredients = [];

        for (let i = 0; i < rec_ingr_list.length; i++) {
            if (rec_ingr_list[i].recipe_id === rec_id) {
                let rec_ingr = rec_ingr_list[i];
                let qty_id = rec_ingr.measurement_qty_id;
                let qty = "";
                let unit_id = rec_ingr.measurement_unit_id;
                let unit = "";
                let ingr_id = rec_ingr.ingredient_id;
                let ingr_name = "";
                let opt = rec_ingr.optional;

                // Loop over quantity list to get quantity value if there is a quantity id
                if (qty_id !== "") {
                    for (let j = 0; j < qty_list.length; j++) {
                        if (qty_list[j].id === qty_id) {
                            qty = qty_list[j].qty;
                            break;
                        }
                    }
                }
                // Loop over units list to get unit value if there is a unit id
                if (unit_id !== "") {
                    for (let k = 0; k < unit_list.length; k++) {
                        if (unit_list[k].id === unit_id) {
                            unit = unit_list[k].unit;
                            break;
                        }
                    }   
                }
                // Loop over ingredients list to get ingredient name
                for (let l = 0; l < ingr_list.length; l++) {
                    if (ingr_list[l].id === ingr_id) {
                        ingr_name = ingr_list[l].name;
                        break;
                    }
                }

                // Create an ingredient object and push it to the ingredients list
                let ingr_obj = {
                    qty: {
                        id: qty_id,
                        qty: qty
                    },
                    unit: {
                        id: unit_id,
                        unit: unit
                    },
                    ingr: {
                        id: ingr_id,
                        name: ingr_name
                    },
                    optional: opt
                };

                ingredients.push(ingr_obj);
            }
        }
        this.setState({ingredients: ingredients})
    }

    // Adds the current recipe to the favourites list and stores all required recipe data in localStorage
    addToFavourites() {
        // Get recipe id from component state
        let id = this.state.rec_id;
        // Get recipe ingredients from component state
        let recipeIngredients = this.state.ingredients;
        // Get existing favourites data from local storage
        let favList = JSON.parse(localStorage.getItem('favList'));
        let localRecipes = JSON.parse(localStorage.getItem('recipes'));
        let localImages = JSON.parse(localStorage.getItem('images'));
        let localRecipesIngredients = JSON.parse(localStorage.getItem('recipesIngredients'));
        let localIngredients = JSON.parse(localStorage.getItem('ingredients'));
        let localUnits = JSON.parse(localStorage.getItem('units'));
        let localQuantities = JSON.parse(localStorage.getItem('quantities'));
        // Get recipe object data from component state
        let recipe = {
            "id": this.state.rec_id,
            "user_id": this.state.user_id,
            "title": this.state.title,
            "description": this.state.description,
            "prep_time": this.state.prep_time,
            "cook_time": this.state.cook_time,
            "servings": this.state.servings,
            "method": this.state.method,
            "image_id": this.state.image_id
        };
        // Get image object data from component state
        let image = {
            "id": this.state.image_id,
            "image_path": this.state.image_path,
            "description": this.state.image_description
        };
        // Initialise empty arrays for deconstructed ingredients array data
        let recipe_ingredient = [];
        let quantities = [];
        let units = [];
        let ingredients = [];

        // Loop through the current recipe ingredients array
        for (let i = 0; i < recipeIngredients.length; i++) {
            // Reconstruct the data for a recipe_ingredient object
            let rec_ingr = {
                "recipe_id": this.state.rec_id,
                "measurement_qty_id": recipeIngredients[i].qty.id,
                "measurement_unit_id": recipeIngredients[i].unit.id,
                "ingredient_id": recipeIngredients[i].ingr.id,
                "optional": recipeIngredients[i].optional
            };
            // Push rec_ingr object to recipe_ingredient array
            recipe_ingredient.push(rec_ingr);

            // Quantity
            // If localQuantities is an array with a length greater than 0
            // Loop through localQuantities and check if it already has the quantity
            // If it does then boolean hasQty set to true
            // If hasQty is false then add the new quantity to quantities array
            // 
            // If localQuantities is not an array repeat this process against the quantities array
            if (Array.isArray(localQuantities) && localQuantities.length) {
                let hasQty = false;
                // Check if qty already in localStorage
                for (let j = 0; j < localQuantities.length; j++) {
                    if (localQuantities[j].id === recipeIngredients[i].qty.id) {
                        hasQty = true;
                        break;
                    }
                }
                // If qty not in localStorage then push it to the quantities array
                if (!hasQty) {
                    let quantity = {
                        "id": recipeIngredients[i].qty.id,
                        "qty": recipeIngredients[i].qty.qty
                    };
                    quantities.push(quantity);
                }
                // If localStorage quantities is empty
            } else {
                // Check if quantities has any elements
                if (quantities.length > 0) {
                    let hasQty = false;
                    // Loop through quantities array and check if qty is present
                    for (let j = 0; j < quantities.length; j++) {
                        if (quantities[j].id === recipeIngredients[i].qty.id) {
                            hasQty = true;
                            break;
                        }
                    }
                    // If qty has not in the quantities array then push new qty to it
                    if (!hasQty) {
                        let quantity = {
                            "id": recipeIngredients[i].qty.id,
                            "qty": recipeIngredients[i].qty.qty
                        };
                        quantities.push(quantity);
                    }
                    // If quantities is also empty then push new quantity object to quantities array
                } else {
                    let quantity = {
                        "id": recipeIngredients[i].qty.id,
                        "qty": recipeIngredients[i].qty.qty
                    };
                    quantities.push(quantity);
                }
            }

            // Unit
            // If localUnits is an array with a length greater than 0
            // Loop through localUnits and check if it already has the unit
            // If it does then boolean hasUnit set to true
            // If hasUnit is false then add the new unit to units array
            // 
            // If localUnits is not an array repeat this process against the units array
            if (Array.isArray(localUnits) && localUnits.length) {
                let hasUnit = false;
                for (let j = 0; j < localUnits.length; j++) {
                    if (localUnits[j].id === recipeIngredients[i].unit.id) {
                        hasUnit = true;
                        break;
                    }
                }
                if (!hasUnit) {
                    let unit = {
                        "id": recipeIngredients[i].unit.id,
                        "unit": recipeIngredients[i].unit.unit
                    };
                    units.push(unit);
                }
            } else {
                if (units.length > 0) {
                    let hasUnit = false;
                    for (let j = 0; j < units.length; j++) {
                        if (units[j].id === recipeIngredients[i].unit.id) {
                            hasUnit = true;
                            break;
                        }
                    }
                    if (!hasUnit) {
                        let unit = {
                            "id": recipeIngredients[i].unit.id,
                            "unit": recipeIngredients[i].unit.unit
                        };
                        units.push(unit);
                    }
                } else {
                    let unit = {
                        "id": recipeIngredients[i].unit.id,
                        "unit": recipeIngredients[i].unit.unit
                    };
                    units.push(unit);
                }
            }

            // Ingredient
            // If localIngredients is an array with a length greater than 0
            // Loop through localIngredients and check if it already has the ingredient
            // If it does then boolean hasIngr set to true
            // If hasIngr is false then add the new ingredient to ingredients array
            // 
            // If localIngredients is not an array repeat this process against the units array
            if (Array.isArray(localIngredients) && localIngredients.length) {
                let hasIngr = false;
                for (let j = 0; j < localIngredients.length; j++) {
                    if (localIngredients[j].id === recipeIngredients[i].ingr.id) {
                        hasIngr = true;
                        break;
                    }
                }
                if (!hasIngr) {
                    let ingredient = {
                        "id": recipeIngredients[i].ingr.id,
                        "name": recipeIngredients[i].ingr.name
                    };
                    ingredients.push(ingredient);
                }
            } else {
                if (ingredients.length > 0) {
                    let hasIngr = false;
                    for (let j = 0; j < ingredients.length; j++) {
                        if (ingredients[j].id === recipeIngredients[i].ingr.id) {
                            hasIngr = true;
                            break;
                        }
                    }
                    if (!hasIngr) {
                        let ingredient = {
                            "id": recipeIngredients[i].ingr.id,
                            "name": recipeIngredients[i].ingr.name
                        };
                        ingredients.push(ingredient);
                    }
                } else {
                    let ingredient = {
                        "id": recipeIngredients[i].ingr.id,
                        "name": recipeIngredients[i].ingr.name
                    };
                    ingredients.push(ingredient);
                }
            }
        }

        // Push current recipe id to favourites list
        if (Array.isArray(favList) && favList.length) {
            favList.push(id);
        } else {
            // If favList not an array then initialise an empty array
            favList = [];
            // Push recipe id to empty favList array
            favList.push(id);
        }
        // Stringify updated favourites list and set to local storage
        localStorage.setItem('favList', JSON.stringify(favList));


        // Push current recipe to recipes list
        if (Array.isArray(localRecipes) && localRecipes.length) {
            localRecipes.push(recipe);
        } else {
            localRecipes = [];
            localRecipes.push(recipe);
        }
        // Stringify updated recipes list and set to local storage
        localStorage.setItem('recipes', JSON.stringify(localRecipes));

        // Push current image to images list
        if (Array.isArray(localImages) && localImages.length) {    
            localImages.push(image);
        } else {
            localImages = [];
            localImages.push(image);
        }
        // Stringify updated images list and set to local storage
        localStorage.setItem('images', JSON.stringify(localImages));

        // Push current recipe ingredients list to recipes ingredients list
        let newRecipesIngredients;
        if (Array.isArray(localRecipesIngredients) && localRecipesIngredients.length) {
            newRecipesIngredients = localRecipesIngredients.concat(recipe_ingredient);
        } else {
            newRecipesIngredients = recipe_ingredient;
        }
        // Stringify updated recipesIngredients list and set to local storage
        localStorage.setItem('recipesIngredients', JSON.stringify(newRecipesIngredients));

        // Push current quantities to local quantities list
        let newQuantities;
        if (Array.isArray(localQuantities) && localQuantities.length) {
            newQuantities = localQuantities.concat(quantities);
        } else {
            newQuantities = quantities;
        }
        // Stringify updated local quantities list and set to local storage
        localStorage.setItem('quantities', JSON.stringify(newQuantities));

        // Push current units to local units list
        let newUnits;
        if (Array.isArray(localUnits) && localUnits.length) {
            newUnits = localUnits.concat(units);
        } else {
            newUnits = units;
        }
        // Stringify updated local units list and set to local storage
        localStorage.setItem('units', JSON.stringify(newUnits));

        // Push current ingredients to local ingredients list
        let newIngredients;
        if (Array.isArray(localIngredients) && localIngredients.length) {
            newIngredients = localIngredients.concat(ingredients);
        } else {
            newIngredients = ingredients;
        }
        // Stringify updated local ingredients list and set to local storage
        localStorage.setItem('ingredients', JSON.stringify(newIngredients));
    }

    removeFavourite() {
        let recId = this.props.match.params.id;
        // Get data from local storage
        let favList = JSON.parse(localStorage.getItem('favList'));
        let recipes = JSON.parse(localStorage.getItem('recipes'));
        let images = JSON.parse(localStorage.getItem('images'));
        let recipesIngredients = JSON.parse(localStorage.getItem('recipesIngredients'));
        let quantities = JSON.parse(localStorage.getItem('quantities'));
        let units = JSON.parse(localStorage.getItem('units'));
        let ingredients = JSON.parse(localStorage.getItem('ingredients'));
        let imgId;

        // Remove the recipe_ingredient for the recipe.
        // If an ingredient is not associated with any 
        // of the remaining recipe_ingredient tables then
        // remove it.
        
        // Remove from favList
        for (let i = 0; i < favList.length; i++) {
            if (favList[i] === recId) {
                favList.splice(i, 1);
                break;
            }
        }

        // Remove recipe
        for (let i = 0; i < recipes.length; i++) {
            if (recipes[i].id === recId) {
                // Get image id of recipe
                imgId = recipes[i].image_id;
                // Remove recipe from recipes list
                recipes.splice(i, 1);
                // End the loop
                break;
            }
        }

        // Remove image
        for (let i = 0; i < images.length; i++) {
            if (images[i].id === imgId) {
                images.splice(i, 1);
                break;
            }
        }

        // Remove associated recipe ingredients
        for (let i = recipesIngredients.length - 1; i >= 0; i--) {
            if (recipesIngredients[i].recipe_id === recId) {
                recipesIngredients.splice(i, 1);
            }
        }

        // Remove associated quantities (If not used by other favourite recipes)
        // Reverse loop splices array elements from array end to prevent error with loop index
        for (let i = quantities.length - 1; i >= 0; i--) {
            let isUsed = false;
            for (let j = 0; j < recipesIngredients.length; j++) {
                if (quantities[i].id === recipesIngredients[j].measurement_qty_id) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                quantities.splice(i, 1);
            }
        }

        // Remove associated units (If not used by other favourite recipes)
        // Reverse loop splices array elements from array end to prevent error with loop index
        for (let i = units.length - 1; i >= 0; i--) {
            let isUsed = false;
            for (let j = 0; j < recipesIngredients.length; j++) {
                if (units[i].id === recipesIngredients[j].measurement_unit_id) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                units.splice(i, 1);
            }
        }

        // Remove associated ingredients (If not used by other favourite recipes)
        // Reverse loop splices array elements from array end to prevent error with loop index
        for (let i = ingredients.length - 1; i >= 0; i--) {
            let isUsed = false;
            for (let j = 0; j < recipesIngredients.length; j++) {
                if (ingredients[i].id === recipesIngredients[j].ingredient_id) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                ingredients.splice(i, 1);
            }
        }
    
        // Stringify updated favourites list and set to local storage
        localStorage.setItem('favList', JSON.stringify(favList));
        // Recipe
        localStorage.setItem('recipes', JSON.stringify(recipes));
        // Image
        localStorage.setItem('images', JSON.stringify(images));
        // Recipe ingredients
        localStorage.setItem('recipesIngredients', JSON.stringify(recipesIngredients));
        // Quantities
        localStorage.setItem('quantities', JSON.stringify(quantities));
        // Units
        localStorage.setItem('units', JSON.stringify(units));
        // Ingredients
        localStorage.setItem('ingredients', JSON.stringify(ingredients));
    }

    // Handles input for 'Favourite' and 'Unfavourite' button
    // Will call removeFavourite() if isFav is true and addToFavourites if isFav is false
    // Switches the state of isFav boolean to the opposite of its current value
    handleClickFavourite() {
        if (this.state.isFav) {
            this.removeFavourite();
        } else {
            this.addToFavourites();
        }
        this.setState(state => ({
            isFav: !state.isFav
        }));
    }

    // Renders view recipe component
    render() {
        return (
            <main className="view-recipe-container">
                {/* Recipe title */}
                <h1>{this.state.title}</h1>
                {/* Image, description, times and servings */}
                <LeadingInfo 
                    image_path={this.state.image_path}
                    image_description={this.state.image_description}
                    description={this.state.description}
                    prep_time={this.state.prep_time}
                    cook_time={this.state.cook_time}
                    servings={this.state.servings}
                />
                {/* Favourite and Unfavourite buttons */}
                <div className="btn-container">
                    {this.state.isFav 
                        ? <button onClick={this.handleClickFavourite} className="action-btn unfavourite-btn">Unfavourite</button>
                        : <button onClick={this.handleClickFavourite} className="action-btn favourite-btn">Favourite</button>
                    }
                </div>
                {/* Ingredients list */}
                <Ingredients ingredients={this.state.ingredients} />
                {/* Method details */}
                <Method method={this.state.method} />
            </main>
        );
    }
}

export default withRouter(ViewRecipe);