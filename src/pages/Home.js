// Import React framework
import React from 'react';

// Import React Router
import { Link } from 'react-router-dom';

// Imports custom hook for fetching JSON data
import useFetchJSON from '../hooks/FetchJSON.js';

// Import logo image
import logo from '../quacking-logo.svg';

// Component renders the preview favourites list.
function RenderPreviewFavourites(props) {
    // Assign component props to variables to minimise repeated code.
    let recipes = props.recipes;
    let images = props.images;
    // Stores rendered favourite recipe preview sections
    let previewList = [];
    
    // Loop over the favourite recipes list.
    for (let i = 0; i < recipes.length; i++) {
        // For each recipe loop over the recipe images to find its image.
        for (let j = 0; j < images.length; j++) {
            // If the image is for the recipe then render the recipe section.
            if (recipes[i].image_id === images[j].id) {
                // Push the rendered recipe section to the previewList.
                previewList.push(
                    <section key={recipes[i].id}>
                        <Link to={"/recipe/" + recipes[i].id}>
                            <img src={images[j].image_path} alt={images[j].description} />
                            <h3>{recipes[i].title}</h3>
                        </Link>
                    </section>
                );
                // Ends the image loop once image has been found.
                break;
            }
        }
        // Prevents more than 6 recipes being rendered for the preview.
        if (i >= 5) {
            // Return current rendered preview list.
            return previewList;
        }
    }
    // If the favourites list has fewer then 6 recipes rendered list will be returned here.
    return previewList;
}

// Parent component for preview favourites list
// Renders either empty favourites list message or list if there is any content.
// Gets favourites list data from localStorage 
class PreviewFavourites extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasFavs: false,
            recipes: [],
            images: []
        }
    }

    // Get favourites list data when the component has mounted
    componentDidMount() {
        let recipes = JSON.parse(localStorage.getItem('recipes'));
        let images = JSON.parse(localStorage.getItem('images'));
        if (Array.isArray(recipes) && recipes.length) {
            this.setState({
                hasFavs: true,
                recipes: recipes,
                images: images
            });
        }
    }

    // Renders the favourites list or empty favourites list message if state hasFavs is false
    render() {
        if (this.state.hasFavs) {
            return (
                <RenderPreviewFavourites recipes={this.state.recipes} images={this.state.images} />
            );
        } else {
            return (
                <section className="preview-favourites-empty"><p>Your favourites list is empty. Why not try adding some recipes? View a recipe and select the Favourite button to start adding to your collection!</p><div><Link to="/category">Find Recipes</Link></div></section>
            );
        }
    }
}

// Renders the shopping list items list
function RenderPreviewItemsList(props) {
    let items = props.items;
    let previewItems = [];
    for (let i = 0; i < items.length; i++) {
        previewItems.push(
            <li key={i}>
                {items[i].qty + items[i].unit.unit + " " + items[i].ingr.name}
            </li>
        );
    }
    return previewItems;
}

// Renders the shopping list or empty list message
// Gets shopping list data from localStorage
class PreviewShoppingList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasItems: false,
            items: []
        };
    }

    // When component mounts get the shopping list data from localStroage
    componentDidMount() {
        let shopLsItems = JSON.parse(localStorage.getItem('shopLsItems'));
        if (Array.isArray(shopLsItems) && shopLsItems.length) {
            this.setState({ 
                hasItems: true,
                items: shopLsItems 
            });
        }
    }
    
    // Renders either the shopping list if state hasItems is true or the empty list message
    render() {
        return (
            <section className="preview-shop-ls">
                {this.state.hasItems
                    ? <ul><RenderPreviewItemsList items={this.state.items} /></ul>
                    : <section><p>Your shopping list is empty. You can add recipe ingredients to your shopping list from any recipe page by selecting the Add To Shopping List button.</p></section>
                }
            </section>
        );
    }
}


// Renders the featured recipes section content
function FeaturedRecipes(props) {
    let recipes = props.recipes;
    let images = props.images;
    let recipesList = [];

    for (let i = 0; i < recipes.length; i++) {
        for (let j = 0; j < images.length; j++) {
            if (recipes[i].image_id === images[j].id) {
                recipesList.push(
                    <section className="feature-recipe" key={recipes[i].id}>
                        <Link to={"/recipe/" + recipes[i].id}>
                            <img src={images[j].image_path} alt={images[j].description} />
                            <h3>{recipes[i].title}</h3>
                        </Link>
                    </section>
                );
                break;
            }
        }
    }

    return recipesList;
}

// Home page content
class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            recipes: [],
            images: [],
            featuredRecipes: ["recipe-8", "recipe-7", "recipe-6", "recipe-1", "recipe-5"]
        }
    }
    async componentDidMount() {
        // Fetch recipe.json from online repository
        const recipes = await useFetchJSON('recipe');

        // Get featured recipes data and store it in recipes state
        this.getFeaturedRecipes(recipes);

        // Fetch image information from image.json 
        const images = await useFetchJSON('image');

        // Get images for featured recipes and update state
        this.getImages(images);
    }

    // Loops through recipes JSON data to get recipe data for featured recipes
    getFeaturedRecipes(recipes) {
        let featuredRecipes = this.state.featuredRecipes;
        // Holds featured recipes data
        let filteredRecipes = [];

        // If a recipe id is in the featured recipes list then add its data to the array
        for (let i = 0; i < featuredRecipes.length; i++) {
            for (let j = 0; j < recipes.length; j++) {
                if (featuredRecipes[i] === recipes[j].id) {
                    filteredRecipes.push(recipes[j]);
                }
            }
        }
        // Updates component state with featured recipes data
        this.setState({ recipes: filteredRecipes });
    }

    // Filters the images JSON to get the featured recipes images
    getImages(images) {
        let recipes = this.state.recipes;
        let filteredImages = [];

        // Checks if a featured recipes image id is the same as the current image in the loop
        // Adds the image to the filteredImages array if they are a match
        for (let i = 0; i < recipes.length; i++) {
            for (let j = 0; j < images.length; j++) {
                if (recipes[i].image_id === images[j].id) {
                    filteredImages.push(images[j]);
                    break;
                }
            }
        }
        // Updates component state with the featured recipes images data
        this.setState({ images: filteredImages });
    }

    // Renders the home page content and child components
    render() {
        let recipes = this.state.recipes;
        let images = this.state.images;

        return (
            <main>
                {/* Top banner */}
                <section className="home-banner">
                    <h1>Let's Get Quacking!</h1>
                    <img src={logo} className="App-logo" alt="logo" />
                    <div><Link to="/category">Explore Our Recipes</Link></div>
                </section>
                {/* Featured Recipes Section */}
                <section className="featured-container">
                    <h2 className="featured-title">Featured Recipes</h2>
                    <FeaturedRecipes recipes={recipes} images={images} />
                    <Link className="" to="/category">Find More Recipes</Link>
                </section>
                {/* Shopping list preview */}
                <section className="preview-shop-ls-container">
                    <div>
                        <h2>Shopping List</h2>
                        <Link to="/shopping-list">Edit</Link>
                    </div>
                    <PreviewShoppingList />
                </section>
                {/* Favourites list preview */}
                <section className="featured-container">
                    <h2>Favourites</h2>
                    <PreviewFavourites />
                    <Link to="/favourites">View All Favourites</Link>
                </section>
            </main>
        );
    }
}

export default Home;