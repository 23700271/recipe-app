import React from 'react';
import { Redirect, withRouter } from 'react-router-dom';

class EditItemForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            itemId: this.props.match.params.id,
            shopLsItems: [],
            item: {
                ingr: {
                    id: "",
                    name: ""
                },
                unit: {
                    id: "",
                    unit: ""
                },
                qty: 0
            },
            value: 0,
            hasSubmit: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        let shopLsItems = JSON.parse(localStorage.getItem('shopLsItems'));
        const itemId = this.state.itemId;
        if (Array.isArray(shopLsItems) && shopLsItems.length) {
            for (let i = 0; i < shopLsItems.length; i++) {
                if (shopLsItems[i].ingr.id === itemId) {
                    this.setState({ 
                        item: shopLsItems[i],
                        value: shopLsItems[i].qty
                    });
                }
            }  
            this.setState({ shopLsItems: shopLsItems });
        } else {
            return <Redirect to='/shopping-list' />;
        }
    }
    
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        let shopLsItems = this.state.shopLsItems;
        let itemId = this.state.itemId;
        for (let i = 0; i < shopLsItems.length; i++) {
            if (shopLsItems[i].ingr.id === itemId) {
                shopLsItems[i].qty = parseFloat(this.state.value);
                break;
            }
        }
        localStorage.setItem('shopLsItems', JSON.stringify(shopLsItems));
        this.hasSubmit();
        event.preventDefault();
        return <Redirect to='/shopping-list' />;
    }

    handleDelete(event) {
        let shopLsItems = this.state.shopLsItems;
        let itemId = this.state.itemId;
        for (let i = 0; i < shopLsItems.length; i++) {
            if (shopLsItems[i].ingr.id === itemId) {
                shopLsItems.splice(i, 1);
                break;
            }
        }
        localStorage.setItem('shopLsItems', JSON.stringify(shopLsItems));
        this.hasSubmit();
        event.preventDefault();
    }

    hasSubmit() {
        this.setState(state => ({
            hasSubmit: !state.hasSubmit
        }));
    }

    render() {
        let item = this.state.item;
        let hasSubmit = this.state.hasSubmit;

        if (hasSubmit) {
            return <Redirect to='/shopping-list' />;
        }
        
        return (
            <section className="pg-edit-item">
                <h2>Edit Item</h2>
                <form className="edit-item-form" onSubmit={this.handleSubmit}>
                    <section className="form-field-container">
                        <label htmlFor="qty">Qty:</label>
                        <p>
                            <input className="input-qty" type="number" name="qty" value={this.state.value} onChange={this.handleChange}>
                            </input> {item.unit.unit + " " + item.ingr.name}
                        </p>                        
                    </section>

                    <div className="form-btns-container">
                        <button className="form-btn remove-item-btn" onClick={this.handleDelete}>Remove Item</button>
                        <input className="form-btn save-item-btn" type="submit" value="Save Changes" />
                    </div>

                </form>
            </section>
        );
    }
}

export default withRouter(EditItemForm);