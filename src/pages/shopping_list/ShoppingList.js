import React from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import EditItemForm from './EditShopLsItem';

function ItemsList(props) {
    let items = props.items;
    let renderedItems = [];
    for (let i = 0; i < items.length; i++) {
        renderedItems.push(
            <li key={i}>
                <Link to={"/shopping-list/edit/" + items[i].ingr.id}>
                    {items[i].qty + items[i].unit.unit + " " + items[i].ingr.name}
                </Link>
            </li>
        );
    }
    return renderedItems;
}

// Parent component of shopping list feature
class ShoppingList extends React.Component {
    constructor() {
        super();
        this.removeAll = this.removeAll.bind(this);
        this.state = {
            items: [],
        }
    }

    // When component finishes initial load
    // Get the shoppig list items from local storage and set the state if there are any items
    componentDidMount() {
        let shopLsItems = JSON.parse(localStorage.getItem('shopLsItems'));
        if (Array.isArray(shopLsItems) && shopLsItems.length) {
            this.setState({ items: shopLsItems });
        }
    }
    
    // Removes all shopping list items from local storage and component state
    removeAll() {
        localStorage.removeItem('shopLsItems');
        this.setState({ items: [] });
    }

    render() {
        let items = this.state.items;
        if (items.length < 1) {
            return (
            <section className="empty-shopping-list">
                <p>Your shopping list is empty.</p>
            </section>
            );
        }
        return (
            <section>
                <ul className="shopping-items-list"><ItemsList items={items} /></ul>
                <div className="btn-container">
                    <button className="shop-ls-btn" onClick={this.removeAll}>Remove All</button>
                </div>
            </section>
        );
    }
}

function ShoppingListPage() {
    return (
        <main>
            <h1>Shopping List</h1>
            <Switch>
                <Route path="/shopping-list/edit/:id">
                    <EditItemForm />
                </Route>
                <Route path="/shopping-list">
                    <ShoppingList />
                </Route>
            </Switch>
        </main>
    );
}

export default ShoppingListPage;