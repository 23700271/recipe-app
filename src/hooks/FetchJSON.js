// import { useState, useEffect } from 'react';

// Custom hook for fetching requested JSON data from online repository.
// Prop request is used to determine the requested JSON file.
async function useFetchJSON(request) {
    // Fetch JSON string response from online repository
    const response = await fetch('https://bitbucket.org/23700271/json_data/raw/d71fd985545550f3bcc6afbf103e9a7b9524416a/' + request + '.json');
    // Parse JSON data string response into JavaScript object
    const json = await response.json();

    // Return parsed JSON data
    return json;
}

// Export useFetchJSON function for use in other components.
export default useFetchJSON;