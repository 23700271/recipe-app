// Import React framework
import React from 'react';

// Import React Router
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

// Import stylesheet
import './App.css';

// Import pages
import Header from './elements/Header.js';
import Footer from './elements/Footer.js';
import Home from './pages/Home.js';
import Favourites from './pages/Favourites.js';
import ShoppingList from './pages/shopping_list/ShoppingList.js';
import RecipeCategoryOverview from './pages/recipes/RecipeCategoryOverview.js';
import BrowseRecipeCategory from './pages/recipes/BrowseRecipeCategory.js';
import ViewRecipe from './pages/recipes/ViewRecipe.js';
import ScrollToTop from './ScrollToTop';

// App component handles React Router route switching.
// URL paths determine component to render
function App() {
  return (
      <Router>
        <ScrollToTop />
        <Header />
        <Switch>
          <Route path="/recipe/:id">
            <ViewRecipe />
          </Route>
          <Route path="/category/:id">
            <BrowseRecipeCategory />
          </Route>
          <Route path="/category">
            <RecipeCategoryOverview />
          </Route>
          <Route path="/favourites">
            <Favourites />
          </Route>
          <Route path="/shopping-list">
            <ShoppingList />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
        <Footer />
      </Router>
  );
}

export default App;
