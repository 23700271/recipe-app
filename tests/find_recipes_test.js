Feature('find recipes');

// Home page to recipes page to browse recipes scenarios
// Browse recipes from the quick recipes category
Scenario('browse the quick recipes category', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    I.see('Favourites', 'h2');
    // And
    I.click('Recipes', 'nav');

    // Then
    I.amOnPage('/category');
    I.see('Recipe Categories', 'h1');
    I.see('Quick', 'h2');
    I.see('Recipes for when you want a meal fast! Ready in 30 minutes or less!', 'p');
    I.see('Easy', 'h2');
    I.see('Simple recipes that anyone can make! Minimal fiddling required!', 'p');
    I.see('Budget', 'h2');
    I.see('Save money with these tasty recipes! Healthy and affordable!', 'p');

    // And
    I.click('Quick', 'h2');

    // Then
    I.amOnPage('/category/cat-0');
    I.see('Quick Recipes', 'h1');
    I.see('Cheese and ham omelette', 'h3');
    I.see('Poached eggs', 'h3');
    I.see('Crunchy banana yoghurt', 'h3');
    I.see('Bacon eggy bread', 'h3');
});