Feature('favourites list');

Scenario('add a recipe to the favourites list', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.dontSee('Cheese and ham omelette');

    // And
    I.amOnPage('/recipe/recipe-0');
    I.see('Favourite', 'button');
    I.dontSee('Unfavourite', 'button');
    // Then
    I.click('Favourite', 'button');
    I.see('Unfavourite', 'button');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.see('Cheese and ham omelette', 'h3');
    // And
    I.click('Cheese and ham omelette', 'h3');

    // Then
    I.amOnPage('/recipe/recipe-0');
    I.see('Cheese and ham omelette', 'h1');
});

Scenario('remove a favourite recipe from the favourites list from recipe page', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.dontSee('Cheese and ham omelette');

    // And
    I.amOnPage('/recipe/recipe-0');
    I.see('Favourite', 'button');
    I.dontSee('Unfavourite', 'button');
    // Then
    I.click('Favourite', 'button');
    I.see('Unfavourite', 'button');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.see('Cheese and ham omelette', 'h3');
    // And
    I.click('Cheese and ham omelette', 'h3');

    // Then
    I.amOnPage('/recipe/recipe-0');
    I.see('Cheese and ham omelette', 'h1');
    I.see('Unfavourite', 'button');
    // And
    I.click('Unfavourite', 'button');

    // Then
    I.see('Favourite', 'button');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.dontSee('Cheese and ham omelette', 'h3');
});

Scenario('remove a favourite recipe from the favourites list from the favourites page', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.dontSee('Cheese and ham omelette');

    // And
    I.amOnPage('/recipe/recipe-0');
    I.see('Favourite', 'button');
    I.dontSee('Unfavourite', 'button');
    // Then
    I.click('Favourite', 'button');
    I.see('Unfavourite', 'button');
    // And
    I.click('Favourites', 'nav');

    // Then
    I.amOnPage('/favourites');
    I.see('Favourites', 'h1');
    I.see('Cheese and ham omelette', 'h3');
    // And
    I.click('Remove', 'button');

    // Then
    I.dontSee('Cheese and ham omelette', 'h3');
});