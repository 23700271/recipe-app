Feature('view recipe');

Scenario('view a recipe', (I) => {
    // When
    I.amOnPage('/category/cat-0');
    I.see('Quick Recipes', 'h1');
    I.see('Recipes for when you want a meal fast! Ready in 30 minutes or less!');
    // And
    I.click('Cheese and ham omelette');

    // Then
    I.amOnPage('/recipe/recipe-0');
    I.see('Cheese and ham omelette', 'h1');
    I.see('Preparation Time');
    I.see('15 mins');
    I.see('Cooking Time');
    I.see('10 mins');
    I.see('Servings');
    I.see('Try this classic omelette for brunch or serve with lots of green salad for a quick and satisfying dinner.', 'p')
    I.see('Ingredients', 'h2');
    I.see('3 egg(s)', 'li');
    I.see('10g butter', 'li');
    I.see('30g cheddar cheese', 'li');
    I.see('1 thick slice(s) of ham', 'li');
    I.see('salt', 'li');
    I.see('black pepper', 'li');
    I.see('Method', 'h2');
    I.see('Gently beat the eggs together in a mixing bowl and season, to taste, with salt and pepper.', 'li');
    I.see('Heat the butter in a frying pan until foaming. Pour in the eggs and cook for a few seconds, until the bottom of the omelette is lightly set. Push the set parts of the omelette into the uncooked centre of the omelette. Cook again, until the omelette has set further, then push those set parts into the centre of the omelette again. Repeat the process until the eggs have just set but the omelette is still soft in the centre.', 'li');
    I.see('Put the cheese and three-quarters of the ham in the centre of the omelette and cook until the cheese has melted.', 'li');
    I.see('Increase the heat to high and cook the omelette for a further 30 seconds, or until it browns on the bottom.', 'li');
    I.see('Fold the omelette in half, then remove the pan from the heat and tilt it slightly to move the omelette to the edge of the pan. Slide the omelette onto a serving plate, then shape it into a neat roll. Sprinkle over the remaining ham.', 'li');
});
