Feature('shopping list');

Scenario('add ingredients to shopping list', (I) => {
        // When
        I.amOnPage('/');
        I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
        I.see("Let's Get Quacking!", 'h1');
        I.see('Featured Recipes', 'h2');
        I.see('Shopping List', 'h2');
        // And
        I.click('Shopping List', 'nav');

        // Then
        I.amOnPage('/shopping-list');
        I.see('Shopping List', 'h1');
        I.dontSee('3 egg(s)', 'a');
        I.dontSee('10g butter', 'a');
        I.dontSee('30g cheddar cheese', 'a');
        I.dontSee('1 thick slice(s) of ham', 'a');
        I.dontSee('salt', 'a');
        I.dontSee('black pepper', 'a');

        // Then
        I.amOnPage('/recipe/recipe-0');
        I.see('Cheese and ham omelette', 'h1');
        I.see('Ingredients', 'h2');
        I.see('3 egg(s)', 'li');
        I.see('10g butter', 'li');
        I.see('30g cheddar cheese', 'li');
        I.see('1 thick slice(s) of ham', 'li');
        I.see('salt', 'li');
        I.see('black pepper', 'li');
        // And
        I.click('Add To Shopping List');
        I.see('Added', 'button');
        // Then
        I.click('Shopping List', 'nav');

        // And
        I.amOnPage('/shopping-list');
        I.see('3 egg(s)', 'a');
        I.see('10g butter', 'a');
        I.see('30g cheddar cheese', 'a');
        I.see('1 thick slice(s) of ham', 'a');
        I.see('salt', 'a');
        I.see('black pepper', 'a');
});

Scenario('remove all items from shopping list', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    // And
    I.click('Shopping List', 'nav');

    // Then
    I.amOnPage('/shopping-list');
    I.see('Shopping List', 'h1');
    I.dontSee('3 egg(s)', 'a');
    I.dontSee('10g butter', 'a');
    I.dontSee('30g cheddar cheese', 'a');
    I.dontSee('1 thick slice(s) of ham', 'a');
    I.dontSee('salt', 'a');
    I.dontSee('black pepper', 'a');

    // Then
    I.amOnPage('/recipe/recipe-0');
    I.see('Cheese and ham omelette', 'h1');
    I.see('Ingredients', 'h2');
    I.see('3 egg(s)', 'li');
    I.see('10g butter', 'li');
    I.see('30g cheddar cheese', 'li');
    I.see('1 thick slice(s) of ham', 'li');
    I.see('salt', 'li');
    I.see('black pepper', 'li');
    // And
    I.click('Add To Shopping List');
    I.see('Added', 'button');
    // Then
    I.click('Shopping List', 'nav');

    // And
    I.amOnPage('/shopping-list');
    I.see('3 egg(s)', 'a');
    I.see('10g butter', 'a');
    I.see('30g cheddar cheese', 'a');
    I.see('1 thick slice(s) of ham', 'a');
    I.see('salt', 'a');
    I.see('black pepper', 'a');
    // Then
    I.click('Remove All');
    // And
    I.dontSee('3 egg(s)', 'a');
    I.dontSee('10g butter', 'a');
    I.dontSee('30g cheddar cheese', 'a');
    I.dontSee('1 thick slice(s) of ham', 'a');
    I.dontSee('salt', 'a');
    I.dontSee('black pepper', 'a');
});

Scenario('remove one item from shopping list', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    // And
    I.click('Shopping List', 'nav');

    // Then
    I.amOnPage('/shopping-list');
    I.see('Shopping List', 'h1');
    I.dontSee('3 egg(s)', 'a');
    I.dontSee('10g butter', 'a');
    I.dontSee('30g cheddar cheese', 'a');
    I.dontSee('1 thick slice(s) of ham', 'a');
    I.dontSee('salt', 'a');
    I.dontSee('black pepper', 'a');

    // Then
    I.amOnPage('/recipe/recipe-0');
    I.see('Cheese and ham omelette', 'h1');
    I.see('Ingredients', 'h2');
    I.see('3 egg(s)', 'li');
    I.see('10g butter', 'li');
    I.see('30g cheddar cheese', 'li');
    I.see('1 thick slice(s) of ham', 'li');
    I.see('salt', 'li');
    I.see('black pepper', 'li');
    // And
    I.click('Add To Shopping List');
    I.see('Added', 'button');
    // Then
    I.click('Shopping List', 'nav');

    // And
    I.amOnPage('/shopping-list');
    I.see('3 egg(s)', 'a');
    I.see('10g butter', 'a');
    I.see('30g cheddar cheese', 'a');
    I.see('1 thick slice(s) of ham', 'a');
    I.see('salt', 'a');
    I.see('black pepper', 'a');
    // Then
    I.click('3 egg(s)');

    // And
    I.amOnPage('/shopping-list/edit/ingr-0');
    I.see('Edit Item', 'h2');
    I.see('egg(s)');
    // Then
    I.click('Remove Item');

    // And
    I.amOnPage('/shopping-list');
    I.see('Shopping List', 'h1');
    I.dontSee('3 egg(s)', 'a');
});

Scenario('change item quantity in shopping list', (I) => {
    // When
    I.amOnPage('/');
    I.seeInTitle("Let's Get Quacking! - Quick, easy and affordable recipes for students");
    I.see("Let's Get Quacking!", 'h1');
    I.see('Featured Recipes', 'h2');
    I.see('Shopping List', 'h2');
    // And
    I.click('Shopping List', 'nav');

    // Then
    I.amOnPage('/shopping-list');
    I.see('Shopping List', 'h1');
    I.dontSee('3 egg(s)', 'a');
    I.dontSee('10g butter', 'a');
    I.dontSee('30g cheddar cheese', 'a');
    I.dontSee('1 thick slice(s) of ham', 'a');
    I.dontSee('salt', 'a');
    I.dontSee('black pepper', 'a');

    // Then
    I.amOnPage('/recipe/recipe-0');
    I.see('Cheese and ham omelette', 'h1');
    I.see('Ingredients', 'h2');
    I.see('3 egg(s)', 'li');
    I.see('10g butter', 'li');
    I.see('30g cheddar cheese', 'li');
    I.see('1 thick slice(s) of ham', 'li');
    I.see('salt', 'li');
    I.see('black pepper', 'li');
    // And
    I.click('Add To Shopping List');
    I.see('Added', 'button');
    // Then
    I.click('Shopping List', 'nav');

    // And
    I.amOnPage('/shopping-list');
    I.see('3 egg(s)', 'a');
    I.see('10g butter', 'a');
    I.see('30g cheddar cheese', 'a');
    I.see('1 thick slice(s) of ham', 'a');
    I.see('salt', 'a');
    I.see('black pepper', 'a');
    // Then
    I.click('3 egg(s)');

    // And
    I.amOnPage('/shopping-list/edit/ingr-0');
    I.see('Edit Item', 'h2');
    I.see('egg(s)');
    I.seeInField('qty', '3');
    // Then
    I.fillField('qty', '4');
    // And
    I.click('Save Changes');

    // Then
    I.amOnPage('/shopping-list');
    I.see('Shopping List', 'h1');
    I.see('4 egg(s)', 'a');
});